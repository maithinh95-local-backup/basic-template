package configuration

import (
	"app/libs/crypto"
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"gopkg.in/yaml.v3"
)

type service struct {
	config    Setting
	path      string
	sourceAES string
}

type Service interface {
	GetSetting() *Setting
}

func New() Service {
	return &service{
		config:    Setting{},
		path:      "./config/setting.yaml",
		sourceAES: "./secret",
	}
}

func (ins *service) GetSetting() *Setting {
	if _, err := os.Stat(ins.path); os.IsNotExist(err) {
		if err := os.MkdirAll(filepath.Dir(ins.path), 0755); err != nil {
			panic(err)
		}
		set := Setting{
			WhoAmI: server{
				Domain:       domain{Bind: "0.0.0.0", Port: "8080"},
				ReadTimeout:  29 * time.Second,
				WriteTimeout: 29 * time.Second,
				IdleTimeout:  59 * time.Second,
			},
			GatewayDetector: domain{Host: "127.0.0.1", Port: "10190"},
			MongoDB: mongoSetting{
				Domains: []domain{
					{Host: "127.0.0.1", Port: "27017"},
				},
				DBName:  "facepay-service-DB",
				Timeout: 15 * time.Second,
			},
			LogToFile: "./logs/",
		}

		switch filepath.Ext(ins.path) {
		case "json", ".json":
			body, err := json.Marshal(&set)
			if err != nil {
				panic(err)
			}
			err = ioutil.WriteFile(ins.path, body, 0664)
			if err != nil {
				panic(err)
			}
		case "yaml", ".yaml":
			body, err := yaml.Marshal(&set)
			if err != nil {
				panic(err)
			}
			err = ioutil.WriteFile(ins.path, body, 0664)
			if err != nil {
				panic(err)
			}
		default:
			panic("config file invalid format")
		}
	}

	{

		body, err := ioutil.ReadFile(ins.path)
		if err != nil {
			panic(err)
		}
		switch filepath.Ext(ins.path) {
		case "json", ".json":
			if err := json.Unmarshal(body, &ins.config); err != nil {
				panic(err)
			}
		case "yaml", ".yaml":
			if err := yaml.Unmarshal(body, &ins.config); err != nil {
				panic(err)
			}
		default:
			panic("config file invalid format")
		}
	}

	{
		secretCompare := ins.sourceAES + ".key"
		secretFile := ins.sourceAES + ".pem"
		if _, err := os.Stat(secretCompare); os.IsNotExist(err) {
			panic(err)
		}
		if _, err := os.Stat(secretFile); os.IsNotExist(err) {
			ins.config.MongoDB.SecretKeyAES, err = crypto.AESGenerate256Key()
			if err != nil {
				panic(err)
			}
			if err := ioutil.WriteFile(secretFile, ins.config.MongoDB.SecretKeyAES, 0664); err != nil {
				panic(err)
			}
		} else {
			ins.config.MongoDB.SecretKeyAES, err = ioutil.ReadFile(secretFile)
			if err != nil {
				panic(err)
			}
		}
	}

	return &ins.config
}
