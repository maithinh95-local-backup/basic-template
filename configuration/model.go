package configuration

import (
	"fmt"
	"strings"
	"time"
)

type Setting struct {
	WhoAmI          server       `yaml:"who_am_i" json:"whoAmI"`
	GatewayDetector domain       `yaml:"gateway_detector" json:"gatewayDetector"`
	MongoDB         mongoSetting `yaml:"mongodb" json:"mongoDB"`
	LogToFile       string       `yaml:"log_to_file" json:"logToFile"`
}

func (ins *Setting) Println() {
	fmt.Printf("\n\r%+v\n", ins)
}

type server struct {
	Domain       domain        `json:"domain" yaml:"domain"`
	ReadTimeout  time.Duration `json:"readTimeOut" yaml:"read_timeout"`
	WriteTimeout time.Duration `json:"writeTimeout" yaml:"write_timeout"`
	IdleTimeout  time.Duration `json:"idleTimeout" yaml:"idle_timeout"`
}

type mongoSetting struct {
	Username     string        `json:"username" yaml:"username"`
	URI          string        `json:"-" yaml:"-"`
	PWD          string        `json:"pwd" yaml:"pwd"`
	SecretKeyAES []byte        `json:"-" yaml:"-"`
	Domains      []domain      `json:"domains" yaml:"domains"`
	DBName       string        `json:"db_name" yaml:"db_name"`
	ReplicaSet   string        `json:"replica_set" yaml:"replica_set"`
	Timeout      time.Duration `json:"timeout" yaml:"timeout"`
}

func (ins *mongoSetting) ParseURI() string {

	ins.URI = "mongodb://"

	if len(ins.Username) > 0 {
		var pwd string
		ins.URI += ins.Username + ":"
		if len(ins.PWD) > 0 {
			ins.URI += ins.PWD + "@"
		} else {
			println("\r\nEnter MONGO_PWD:")
			fmt.Scanf("%s", &pwd)
			ins.PWD = pwd
			ins.URI += ins.PWD + "@"
		}
	}

	for i, domain := range ins.Domains {
		if len(domain.Port) == 0 {
			domain.Port = "27017"
		}
		if i != 0 {
			ins.URI += ","
		}
		ins.URI += domain.Host + ":" + domain.Port
	}

	ins.URI += "/" + ins.DBName

	if len(ins.ReplicaSet) > 0 {
		ins.URI += "?replicaSet=" + ins.ReplicaSet
	}

	return ins.URI
}

type domain struct {
	Bind string `yaml:"bind" json:"bind"`
	Host string `yaml:"host" json:"host"`
	Port string `yaml:"port" json:"port"`
}

func (ins *domain) ToAddr() string {
	if len(ins.Host) == 0 {
		ins.Bind = "0.0.0.0"
	}
	if len(ins.Port) == 0 {
		ins.Bind = "80"
	}
	return fmt.Sprintf("%s:%s", ins.Bind, ins.Port)
}

func (ins *domain) ToString() string {
	if len(ins.Host) == 0 {
		ins.Host = "127.0.0.1"
	}
	if !strings.HasPrefix(ins.Host, "http") && !strings.HasPrefix(ins.Host, "ws") {
		ins.Host = "http://" + ins.Host
	}
	if len(ins.Port) == 0 || ins.Port == "80" || ins.Port == "443" {
		return ins.Host
	}
	return fmt.Sprintf("%s:%s", ins.Host, ins.Port)
}

// ToStrings func
func ToStrings(ds []domain) (hosts []string) {
	for _, d := range ds {
		hosts = append(hosts, d.ToString())
	}
	return
}
