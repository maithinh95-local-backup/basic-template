######## Start from the builder golang base image #######
FROM amd64/golang:1.16.3-alpine3.12 as builder

RUN apk --no-cache add tzdata bash
ARG TZ=Asia/Ho_Chi_Minh
RUN ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime;\
    date

# Set the Current Working Directory inside the container
WORKDIR /app/

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app and clean resources
RUN CGO_ENABLED=0\
    GOOS=linux\
    GOARCH=amd64\
    go build --ldflags "-extldflags \"-static\" -s -w" -o bin/application -trimpath ./exec/server/*.go
# --------------------------------------------------------------- #

######## Start from the linux base image #######
FROM alpine:3.12

# Add Maintainer Info
LABEL maintainer="WeeDigital Co., Ltd | thinh@wee.vn"

RUN apk --no-cache add tzdata bash
ARG TZ=Asia/Ho_Chi_Minh
RUN ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime;\
    date

# Set the Current Working Directory inside the container
WORKDIR /foo/

COPY --from=builder /app/bin/application /bin/

VOLUME ["/foo"]

EXPOSE 8080

ENTRYPOINT [ "/bin/bash", "-c", "/bin/application" ]