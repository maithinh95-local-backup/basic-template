package database

import (
	"app/libs/zapc"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/*
MongoIndex object
*/
type MongoIndex struct {
	Keys   interface{}
	Unique bool
}

/*
MongoConnect : create a new connection to mongodb
*/
func MongoConnect(uri, dbname string, timeout time.Duration) ([]*mongo.CollectionSpecification, *mongo.Database, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), timeout)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, err
	}
	db := client.Database(dbname)

	if err := client.Ping(ctx, db.ReadPreference()); err != nil {
		return nil, nil, err
	}

	cspec, err := db.ListCollectionSpecifications(ctx, bson.M{})
	if err != nil {
		return nil, nil, err
	}

	return cspec, db, nil
}

/*
MongoInit : create first collection config
*/
func MongoInit(console zapc.Service, db *mongo.Database, collectionname string, index ...MongoIndex) *mongo.Collection {
	var (
		ctx        context.Context
		cancel     context.CancelFunc
		collection *mongo.Collection

		indexes = []mongo.IndexModel{}
	)

	ctx, cancel = context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	{
		var collectionValidate = func() (created bool) {
			list, err := db.ListCollectionNames(ctx, bson.M{})
			if err != nil {
				console.WithError("ListCollectionNames", err)
				return
			}
			for _, n := range list {
				if n == collectionname {
					created = true
					break
				}
			}
			return
		}
		if created := collectionValidate(); created {
			console.Debug(zapc.ToString("Collection `%s` is already available", collectionname), zapc.Bool("collectionValidate", created))
		} else {
			// err := db.CreateCollection(ctx, collectionname, options.CreateCollection().SetCollation(&options.Collation{
			// 	Locale:        "vi",
			// 	CaseFirst:     "lower",
			// 	Normalization: true,
			// }))
			if err := db.CreateCollection(ctx, collectionname); err != nil {
				console.WithError("Create a new collection failed", err)
			} else {
				console.Debug(zapc.ToString("Collection `%s` is already available", collectionname), zapc.Error(err))
			}

		}
		collection = db.Collection(collectionname)
	}

	{
		for _, uq := range index {
			if uq.Keys == nil {
				continue
			}
			indexes = append(indexes, mongo.IndexModel{
				Keys:    uq.Keys,
				Options: options.Index().SetUnique(uq.Unique),
			})
		}

		if len(indexes) > 0 {
			names, err := collection.Indexes().CreateMany(ctx, indexes)
			if err != nil {
				console.WithError("Creating multiple indexes failed", err)
			}
			for _, name := range names {
				console.Debug("Index created", zapc.String("IndexName", name))
			}
		}
	}

	return collection
}
