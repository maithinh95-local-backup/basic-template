package monitor

import "fmt"

type systemId struct {
	Value int
}

func (ins *systemId) String() string {
	return fmt.Sprintf("%d", ins.Value)
}
