package monitor

import "os"

type service struct {
	gid, uid, ppid, pid *systemId
}

type Service interface {
	GetGroupID() *systemId
	GetUID() *systemId
	GetPPID() *systemId
	GetPID() *systemId
}

func New() Service {
	return &service{
		ppid: new(systemId),
		pid:  new(systemId),
	}
}

func (ins *service) GetGroupID() *systemId {
	if ins.gid.Value == 0 {
		ins.gid.Value = os.Getgid()
	}
	return ins.gid
}

func (ins *service) GetUID() *systemId {
	if ins.uid.Value == 0 {
		ins.uid.Value = os.Getuid()
	}
	return ins.uid
}

func (ins *service) GetPPID() *systemId {
	if ins.ppid.Value == 0 {
		ins.ppid.Value = os.Getppid()
	}
	return ins.ppid
}

func (ins *service) GetPID() *systemId {
	if ins.pid.Value == 0 {
		ins.pid.Value = os.Getpid()
	}
	return ins.pid
}
