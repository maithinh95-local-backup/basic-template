package net

import (
	"app/libs/net/options"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

type Reponse struct {
	PrimaryIndex int
	StatusCode   int
	Header       http.Header
	Body         []byte
}

/*
Curl function.
By default, if the option is NIL then timeout is 30 seconds.
*/
func Curl(url string, option *options.Option) (*Reponse, error) {

	if option == nil {
		option = options.CurlOption()
	}

	if strings.HasPrefix(url, "ws") {
		return nil, errors.WithStack(errors.New("the `websocket` protocol is not supported"))
	}
	if strings.HasPrefix(url, "http") {
		//pass
	} else {
		url = "http://" + url
	}

	if len(option.Params) > 0 {
		url += "/" + option.Params
	}

	request, err := http.NewRequest(option.Method, url, option.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	for key, arr := range option.Header {
		for i, value := range arr {
			if i == 0 {
				request.Header.Set(key, value)
			} else {
				request.Header.Add(key, value)
			}
		}
	}

	client := &http.Client{
		Timeout: option.Timeout,
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	buffer, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	return &Reponse{
		StatusCode: response.StatusCode,
		Header:     response.Header.Clone(),
		Body:       buffer,
	}, errors.WithStack(err)
}
