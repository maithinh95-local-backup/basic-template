package net

import (
	"app/libs/net/options"
)

type lb struct {
	primaryIndex                 int
	urls                         []string
	primaryEntry, secondaryEntry string
}

type LB interface {
	SetPrimaryEntry(path string)
	SetSecondaryEntry(path string)
	Curl(option *options.Option) (*Reponse, error)
	CurlWithAction(secondaryAction func(primaryStatus int, primaryBody []byte, actionURL string), option *options.Option) (*Reponse, error)
}

func NewLoad(urls ...string) LB {
	return &lb{
		primaryIndex: 0,
		urls:         urls,
	}
}

func (ins *lb) SetPrimaryEntry(path string) {
	ins.primaryEntry = path
}

func (ins *lb) SetSecondaryEntry(path string) {
	ins.secondaryEntry = path
}

func (ins *lb) getURL(def int) (int, string, []string) {
	if len(ins.urls) == 0 {
		return -1, "", nil
	}
	if def < 0 || def > len(ins.urls) {
		ins.primaryIndex++
	}
	if len(ins.urls) < ins.primaryIndex {
		ins.primaryIndex = 1
	}

	var (
		primary   string
		secondary []string
	)
	for i, value := range ins.urls {
		if i == ins.primaryIndex-1 {
			primary = value + ins.primaryEntry
		} else {
			secondary = append(secondary, value+ins.secondaryEntry)
		}
	}
	return ins.primaryIndex - 1, primary, secondary
}

func (ins *lb) Curl(option *options.Option) (*Reponse, error) {
	if option == nil {
		option = options.CurlOption()
	}

	primaryIndex, priURL, _ := ins.getURL(option.Must)

	response, err := Curl(priURL, option)
	if err != nil {
		return nil, err
	} else {
		response.PrimaryIndex = primaryIndex
	}
	return response, nil
}

func (ins *lb) CurlWithAction(secondaryAction func(primaryStatus int, primaryBody []byte, actionURL string), option *options.Option) (*Reponse, error) {
	if option == nil {
		option = options.CurlOption()
	}

	primaryIndex, priURL, secURL := ins.getURL(option.Must)

	response, err := Curl(priURL, option)
	if err != nil {
		return nil, err
	} else {
		response.PrimaryIndex = primaryIndex
		for _, url := range secURL {
			go secondaryAction(response.StatusCode, response.Body, url)
		}
	}
	return response, err
}
