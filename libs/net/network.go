package net

import (
	"app/libs/zapc"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

var (
	jsonContentType = []string{"application/json; charset=utf-8"}

// 	jsonpContentType     = []string{"application/javascript; charset=utf-8"}
// 	jsonAsciiContentType = []string{"application/json"}
)

type netWriter struct {
	serviceName string
	r           *http.Request
	w           *bodyWriter // gin.ResponseWriter
	rawReq      interface{}
	errors      []error
}

type bodyWriter struct {
	body *bytes.Buffer
	gin.ResponseWriter
}

type Net interface {
	DefaultQuery(key, def string) string
	DefaultParam(p gin.Params, key, def string) string
	CloneHeader() http.Header
	BasicAuth() (username string, password string, ok bool)
	BindHeader(key string) (value string)
	BindRawData() (body []byte, err error)
	BindJSON(v interface{}) (err error)

	Status(statusCode int)
	Error(err error)
	Write(statusCode int, content string, b []byte) (int, error)
	WriteJSON(statusCode int, v interface{})
	WriteString(s string) (int, error)

	WriteLog(z zapc.Service)
	GetSeviceName() string
}

func NewNetwork(service string, httpReq *http.Request, ginResp gin.ResponseWriter) Net {
	return &netWriter{
		serviceName: service,
		r:           httpReq,
		w: &bodyWriter{
			body:           bytes.NewBuffer(nil),
			ResponseWriter: ginResp,
		},
		rawReq: "compressed",
	}
}

/***********************************/
/******** REQUEST RENDERING ********/
/***********************************/

func (ins *netWriter) DefaultQuery(key, def string) string {
	value := ins.r.URL.Query().Get(key)
	if len(value) == 0 {
		return def
	}
	return value
}

/*
Return value URL of path/{key}
*/
func (ins *netWriter) DefaultParam(p gin.Params, key, def string) string {
	value, ok := p.Get(key)
	if !ok {
		return def
	}
	return value
}

func (ins *netWriter) CloneHeader() http.Header {
	return ins.r.Header.Clone()
}

func (ins *netWriter) BasicAuth() (username string, password string, ok bool) {
	return ins.r.BasicAuth()
}

func (ins *netWriter) BindHeader(key string) (value string) {
	return ins.r.Header.Get(key)
}

func (ins *netWriter) BindRawData() (body []byte, err error) {
	data, err := ioutil.ReadAll(ins.r.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	ins.rawReq = base64.StdEncoding.EncodeToString(data)
	return data, nil
}

func (ins *netWriter) BindJSON(v interface{}) (err error) {
	data, err := ioutil.ReadAll(ins.r.Body)
	if err != nil {
		return errors.WithStack(err)
	}
	ins.rawReq = bytes.NewBuffer(data).String() // v.(interface{})
	return json.Unmarshal(data, &v)
}

/************************************/
/******** RESPONSE RENDERING ********/
/************************************/

func (ins *netWriter) Status(statusCode int) {
	ins.w.WriteHeader(statusCode)
}

func (ins *netWriter) Error(err error) {
	ins.errors = append(ins.errors, err)
}

func (ins *netWriter) Write(statusCode int, content string, b []byte) (int, error) {
	if len(content) > 0 {
		ins.w.Header().Set("Content-Type", content)
	}
	{
		ins.w.WriteHeader(statusCode)
		ins.w.body.Write(b)
	}
	return ins.w.Write(b)
}

func (ins *netWriter) WriteJSON(statusCode int, v interface{}) {
	ins.w.WriteHeader(statusCode)

	if !bodyAllowedForStatus(statusCode) {
		ins.w.WriteHeaderNow()
		return
	}

	jsonBytes, err := json.Marshal(v)
	if err != nil {
		panic(err)
	} else {
		ins.w.Header().Set("Content-Type", jsonContentType[0])
	}
	{
		ins.w.body.Write(jsonBytes)
	}
	if _, err = ins.w.Write(jsonBytes); err != nil {
		panic(err)
	}
}

func (ins *netWriter) WriteString(s string) (int, error) {
	{
		ins.w.body.WriteString(s)
	}
	return ins.w.WriteString(s)
}

/***************************************/
/******** RAW-LOGGING RENDERING ********/
/***************************************/

func (ins *netWriter) WriteLog(z zapc.Service) {
	z.WithFields(
		zapc.Any("headers", map[string]interface{}{
			"requestURL":            ins.r.RequestURI,
			"requestMethod":         ins.r.Method,
			"statusCode":            ins.w.Status(),
			"remoteAddress":         ins.r.RemoteAddr,
			"referrerPolicy":        ins.r.Referer(),
			"responseHeaders":       ins.w.Header(),
			"requestHeaders":        ins.r.Header,
			"queryStringParameters": ins.r.URL.Query(),
		}),
		zapc.Any("request", ins.rawReq),
		zapc.String("response", ins.w.body.String()),
		zapc.Errors("errors", ins.errors...),
	).Debug(ins.serviceName)
}

func (ins *netWriter) GetSeviceName() string {
	return ins.serviceName
}

// bodyAllowedForStatus is a copy of http.bodyAllowedForStatus non-exported function.
func bodyAllowedForStatus(status int) bool {
	switch {
	case status >= 100 && status <= 199:
		return false
	case status == http.StatusNoContent:
		return false
	case status == http.StatusNotModified:
		return false
	}
	return true
}
