package options

import (
	"bytes"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

/*
CurlOption function
*/
func CurlOption() *Option {
	return &Option{
		Method:  http.MethodGet,
		Header:  http.Header{},
		Timeout: 30 * time.Second,
		Must:    -1,
	}
}

/*
SetMethod function
*/
func (ins *Option) SetMethod(method string) error {
	var temp = map[string]bool{
		http.MethodConnect: true,
		http.MethodDelete:  true,
		http.MethodGet:     true,
		http.MethodHead:    true,
		http.MethodOptions: true,
		http.MethodPatch:   true,
		http.MethodPost:    true,
		http.MethodPut:     true,
		http.MethodTrace:   true,
	}

	value, ok := temp[method]
	if ok && value {
		ins.Method = method
		return nil
	}

	return errors.WithStack(errors.New("method invalid"))
}

/*
SetHeader function
*/
func (ins *Option) SetHeader(header map[string][]string) {
	for key, h := range header {
		for i, value := range h {
			if i == 0 {
				ins.Header.Set(key, value)
			} else {
				ins.Header.Add(key, value)
			}
		}
	}
}

/*
AddHeader function
*/
func (ins *Option) AddHeader(key, value string) {
	ins.Header.Add(key, value)
}

/*
SetData function
*/
func (ins *Option) SetData(body []byte) {
	ins.Body = bytes.NewReader(body)
}

/*
SetTimeout function
*/
func (ins *Option) SetTimeout(d time.Duration) {
	ins.Timeout = d
}

/*
SetDeadline function.
Deadline must take longer than 1 second.
*/
func (ins *Option) SetDeadline(t time.Time) {
	go ins.refeshTimeout(t)
}

/*
SetParam function.
*/
func (ins *Option) SetParam(param string) {
	ins.Params = param
}

/*
SetIndex function.
*/
func (ins *Option) SetIndex(index int) {
	ins.Must = index
}

func (ins *Option) refeshTimeout(t time.Time) {
	for {
		if time.Now().After(t) {
			ins.Timeout = 0
			return
		}

		ins.Timeout = time.Until(t)
		time.Sleep(time.Second)
	}
}
