package crypto

import (
	"crypto/sha256"
)

func BytesToHashSha256(data []byte) []byte {
	h := sha256.New()
	h.Write(data)
	hByte := h.Sum(nil)
	return hByte
}

func StringToSha256(text string) []byte {
	textSha256 := sha256.Sum256([]byte(text))
	return textSha256[:]
}

func HashSha256ToString(text string) string {
	textSha256 := sha256.Sum256([]byte(text))
	return string(textSha256[:])
}
