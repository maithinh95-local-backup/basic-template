package crypto

import "crypto/rand"

//AESGenerate256Key : generate an 256 bits AES key.
func AESGenerate256Key() (key []byte, err error) {
	b := make([]byte, 32)
	_, err = rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return key, err
	}
	return b, nil
}
