package zapc

import (
	"io"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type service struct {
	log *zap.Logger
}

type Service interface {
	/*
		Use SugaredLogger with the auto-detect format, but performance will be slower.
	*/
	Sugar() *zap.SugaredLogger
	/*
		Add fields to the logger.
		Then use the logger interface functions to do the logging.
	*/
	WithFields(fields ...zapcore.Field) Service
	/*
		Logging at Error level
	*/
	WithError(msg string, errs ...error)
	/*
		Logging at Fatal level.
		The `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Fatal(msg string, fields ...zapcore.Field)
	/*
		Logging at Panic level.
		the `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Panic(msg string, fields ...zapcore.Field)
	/*
		Logging at DPanic level.
		the `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	DPanic(msg string, fields ...zapcore.Field)
	/*
		Logging at Debug level.
		the `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Debug(msg string, fields ...zapcore.Field)
	/*
		Logging at Info level.
		The `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Info(msg string, fields ...zapcore.Field)
	/*
		Logging at Warn level.
		The `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Warn(msg string, fields ...zapcore.Field)
	/*
		Logging at Error level.
		The `fields` parameter will usually be zap.Any, zap.Binary, zap.Bool, zap.Int, zap.Int32, zap.Int64, zap.Float32, zap.Float64, zap.Error  ...
	*/
	Error(msg string, fields ...zapcore.Field)
}

/*
	Create a new logging interface.
	The `name` parameter is an optional value to distinguish the application/process - possibly empty.
	The `w` Writer parameter is where the log will be written to; it takes the values os.Stdout / os.StdErr - system log output,
	an output of any form of io.Writer; or if you don't want to log - set the value to io.Discard.
*/
func New(pid string, level zapcore.Level, format FormatEncode, w io.Writer) Service {
	var (
		cfg     zapcore.EncoderConfig = zap.NewProductionEncoderConfig()
		encoder zapcore.Encoder       = zapcore.NewJSONEncoder(cfg)
		zws     zapcore.WriteSyncer   = zapcore.AddSync(w)

		core zapcore.Core
	)

	{
		cfg.NameKey = NameKey
		cfg.TimeKey = TimeKey
		cfg.EncodeTime = zapcore.TimeEncoderOfLayout(TimeLayout)
	}

	switch format {
	case JSONEncoder:
		encoder = zapcore.NewJSONEncoder(cfg)
	case ConsoleEncoder:
		encoder = zapcore.NewConsoleEncoder(cfg)
	default:
		encoder = zapcore.NewJSONEncoder(cfg)
	}

	{
		core = zapcore.NewCore(encoder, zws, level)
	}

	return &service{
		log: zap.New(core).Named(pid),
	}
}

func (ins *service) Sugar() *zap.SugaredLogger {
	return ins.log.Sugar()
}

func (ins *service) WithFields(fields ...zapcore.Field) Service {
	return &service{
		log: ins.log.With(fields...),
	}
}

func (ins *service) WithError(msg string, errs ...error) {
	defer ins.log.Sync()
	ins.log.Error(msg, zap.Errors("error", errs))
}

func (ins *service) Fatal(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Fatal(msg, fields...)
}

func (ins *service) Panic(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Panic(msg, fields...)
}

func (ins *service) DPanic(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.DPanic(msg, fields...)
}

func (ins *service) Debug(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Debug(msg, fields...)
}

func (ins *service) Info(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Info(msg, fields...)
}

func (ins *service) Warn(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Warn(msg, fields...)
}

func (ins *service) Error(msg string, fields ...zapcore.Field) {
	defer ins.log.Sync()
	ins.log.Error(msg, fields...)
}
