package zapc

import "go.uber.org/zap/zapcore"

type FormatEncode uint8

const (
	JSONEncoder FormatEncode = iota
	ConsoleEncoder
)

const (
	DebugLevel  zapcore.Level = zapcore.DebugLevel
	InfoLevel   zapcore.Level = zapcore.InfoLevel
	WarnLevel   zapcore.Level = zapcore.WarnLevel
	ErrorLevel  zapcore.Level = zapcore.ErrorLevel
	DPanicLevel zapcore.Level = zapcore.DPanicLevel
	PanicLevel  zapcore.Level = zapcore.PanicLevel
	FatalLevel  zapcore.Level = zapcore.FatalLevel
)

const (
	NameKey    string = "pid"
	TimeKey    string = "time"
	TimeLayout string = "2006-01-02 15:04:05"
)
