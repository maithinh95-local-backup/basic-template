package zapc

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

/*

================================= Public functions ==============================

*/

func NewRotateWriter(path, filename string) io.Writer {
	path, err := filepath.Abs(path)
	if err != nil {
		println("[ERR------]", err.Error())
		return os.Stdout
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			println("[ERR------]", err.Error())
			return os.Stdout
		}
	}

	if len(filename) == 0 {
		filename = "app.log"
	}

	lumb := &lumberjack.Logger{
		Filename:   path + "/" + filename,
		MaxSize:    50, // megabytes
		MaxBackups: 365,
		MaxAge:     1, //days
		LocalTime:  true,
		Compress:   true, // disabled by default
	}

	{
		go rotate(lumb)
	}

	return lumb
}

func Any(key string, value interface{}) zapcore.Field {
	return zap.Any(key, value)
}

func Binary(key string, val []byte) zapcore.Field {
	return zap.Binary(key, val)
}

func Bool(key string, val bool) zapcore.Field {
	return zap.Bool(key, val)
}

func Boolp(key string, val *bool) zapcore.Field {
	return zap.Boolp(key, val)
}

func Bools(key string, val ...bool) zapcore.Field {
	return zap.Bools(key, val)
}

func ByteString(key string, val []byte) zapcore.Field {
	return zap.ByteString(key, val)
}

func ByteStrings(key string, val ...[]byte) zapcore.Field {
	return zap.ByteStrings(key, val)
}

func Duration(key string, val time.Duration) zapcore.Field {
	return zap.Duration(key, val)
}

func Durationp(key string, val *time.Duration) zapcore.Field {
	return zap.Durationp(key, val)
}

func Durations(key string, val ...time.Duration) zapcore.Field {
	return zap.Durations(key, val)
}

func Error(err error) zapcore.Field {
	return zap.Error(err)
}

func Errors(key string, errs ...error) zapcore.Field {
	return zap.Errors(key, errs)
}

func Float32(key string, val float32) zapcore.Field {
	return zap.Float32(key, val)
}

func Float32p(key string, val *float32) zapcore.Field {
	return zap.Float32p(key, val)
}

func Float32s(key string, val ...float32) zapcore.Field {
	return zap.Float32s(key, val)
}

func Float64(key string, val float64) zapcore.Field {
	return zap.Float64(key, val)
}

func Float64p(key string, val *float64) zapcore.Field {
	return zap.Float64p(key, val)
}

func Float64s(key string, val []float64) zapcore.Field {
	return zap.Float64s(key, val)
}

func Int(key string, val int) zapcore.Field {
	return zap.Int(key, val)
}

func Intp(key string, val *int) zapcore.Field {
	return zap.Intp(key, val)
}

func Ints(key string, val ...int) zapcore.Field {
	return zap.Ints(key, val)
}

func Int32(key string, val int32) zapcore.Field {
	return zap.Int32(key, val)
}

func Int32p(key string, val *int32) zapcore.Field {
	return zap.Int32p(key, val)
}

func Int32s(key string, val ...int32) zapcore.Field {
	return zap.Int32s(key, val)
}

func Int64(key string, val int64) zapcore.Field {
	return zap.Int64(key, val)
}

func Int64p(key string, val *int64) zapcore.Field {
	return zap.Int64p(key, val)
}

func Int64s(key string, val ...int64) zapcore.Field {
	return zap.Int64s(key, val)
}

func Stack(key string) zapcore.Field {
	return zap.Stack(key)
}

func String(key string, val string) zapcore.Field {
	return zap.String(key, val)
}

func Stringp(key string, val *string) zapcore.Field {
	return zap.Stringp(key, val)
}

func Strings(key string, val ...string) zapcore.Field {
	return zap.Strings(key, val)
}

func Time(key string, val time.Time) zapcore.Field {
	return zap.Time(key, val)
}

func Timep(key string, val *time.Time) zapcore.Field {
	return zap.Timep(key, val)
}

func Times(key string, val ...time.Time) zapcore.Field {
	return zap.Times(key, val)
}

func ToString(format string, a ...interface{}) string {
	return fmt.Sprintf(format, a...)
}

/*

================================= Private functions ==============================

*/

func rotate(lumb *lumberjack.Logger) {
	var (
		toNewDay     time.Duration
		fileNotFound = func() <-chan bool {
			c := make(chan bool, 1)
			go func() {
				for {
					time.Sleep(1 * time.Second)
					if _, err := os.Stat(lumb.Filename); os.IsNotExist(err) {
						c <- true
					}
				}
			}()
			return c
		}()
	)
	for {
		if err := lumb.Rotate(); err != nil {
			println("[ERR------]", err.Error())
		}
		{
			yyyy, mm, dd := time.Now().Add(24 * time.Hour).Date()
			toNewDay = -time.Since(time.Date(yyyy, mm, dd, 0, 0, 0, 0, time.Local))
		}
		select {
		case <-fileNotFound:
			println("[ZAPC-----] Rotate new file >", lumb.Filename, "- because file not found")
			continue
		case <-time.After(toNewDay):
			println("[ZAPC-----] Rotate new file >", lumb.Filename, "for new day")
			continue
		}
	}
}
