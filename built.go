package app

import (
	"fmt"
	"strings"
)

var (
	Service   string = "TEMPLATE-SERVICE"
	Branch    string = "None"
	Version   string = "None"
	Revision  string = "None"
	BuildUser string = "None"
	BuildDate string = "None"
)

type builtInfo struct {
	ServiceName string `json:"service" mapstructure:"service"` // mapstructure
	Branch      string `json:"branch" mapstructure:"branch"`
	Version     string `json:"version" mapstructure:"version"`
	Revision    string `json:"revision" mapstructure:"revision"`
	BuildUser   string `json:"builder" mapstructure:"builder"`
	BuildDate   string `json:"buildDate" mapstructure:"buildDate"`
}

type built struct {
	info builtInfo
}
type Built interface {
	ShowDisplay() builtInfo
}

func GetBuiltInfomation() Built {
	return &built{
		info: builtInfo{
			ServiceName: Service,
			Branch:      Branch,
			Version:     Version,
			Revision:    Revision,
			BuildUser:   BuildUser,
			BuildDate:   BuildDate,
		},
	}
}

func (ins *built) ShowDisplay() builtInfo {
	printLine := func(nextLine bool, ref string, s ...string) {
		var (
			chars []string
			arr   []string = make([]string, 50)
		)
		if len(ref) > 1 {
			ref = string(ref[0])
		}
		for _, str := range s {
			if len(s) > 0 {
				chars = append(chars, strings.Split(str, "")...)
			}
		}
		if !nextLine {
			for i := range arr {
				if i < 3 || i > len(chars)+2 || len(chars) == 0 {
					arr[i] = ref
				} else {
					arr[i] = chars[i-3]
				}
			}
		}
		fmt.Println(strings.Join(arr, ""))
	}

	printLine(false, "_")
	printLine(true, "")
	printLine(false, " ", "> ServiceName: ", ins.info.ServiceName)
	printLine(false, " ", "> Branch: ", ins.info.Branch)
	printLine(false, " ", "> Version: ", ins.info.Version)
	printLine(false, " ", "> Revision: ", ins.info.Revision)
	printLine(false, " ", "> BuildUser: ", ins.info.BuildUser)
	printLine(false, " ", "> BuildDate: ", ins.info.BuildDate)
	printLine(false, "_")
	printLine(true, "")

	return ins.info
}
