package template

import (
	"encoding/base64"
	"testing"
)

func Test_CheckDataConstants(t *testing.T) {
	if len(faviconIco) == 0 {
		t.Fail()
	}
	if _, err := base64.StdEncoding.DecodeString(faviconIco); err != nil {
		t.Fail()
	}
	if len(healthy) == 0 {
		t.Fail()
	}
	if len(page404) == 0 {
		t.Fail()
	}
}
