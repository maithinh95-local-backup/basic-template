package template

/* base64 encode string */
const faviconIco string = "iVBORw0KGgoAAAANSUhEUgAAACoAAAAfCAMAAACmqq93AAAAh1BMVEUAAAD///////////////////////////////////////8wr/80q/8yrf8zrf8wn/8yrP////////////8zrf8zrf81r/8wr/80rP8yrf8yrf////8wr/////////8yr/////8yrf81rf8zrP8zrP8zrP////////////////////////8zrf/k0jmfAAAAK3RSTlMAABB/74DPnyDfoCBA358Qz4+vkL/vMBDPn+9fMFAwj3B/YFC/kGDeQO7+UmdJEgAAAOtJREFUeNqN0ke2wjAMQFFBHOIQyufTW+hV7H99IMGxYoQxb2brziSIVk8MUmlcNvBNpkiZzEakWMzfrMiYzZBrAlW0qMK3WjJt36hO19lGXUvP/mnbQ+4fIGaT6kPbPnKpL7UdAECTx1qKHbLtkKWxkzlJv1HFOqkWoqySYRuTMJ4wbXdl8Rl8bvCUhZxTyE5Zzh5ynojtheUCYIlINkcu+SJL8wQ2YFdVSa1DdnPjWk6aEipWy60vxaY1Jd1sCZS2uz11WMjkCKCtl0jJnl5/tbDUVklDMmLPR05fsr2IjWVPV+pSwg+dufIOBmZNUv7R6HcAAAAASUVORK5CYII="

/* HTML content */
const healthy string = `
<!DOCTYPE html>
<html lang="vi-VI" data-oe-company-name="WeeDigital Company">
  <head>
    <meta charset="utf-8"/>
    <title>Health status</title>
    <link rel="alternate" hreflang="vi" href="/health"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
  </head>
  <body>
    <div>
      </br>
      <div style="text-align:center;border:0px solid red">
        </br>
        <h1>ARE HEALTHY</h1>
      </div>
    </div>
  </body>
  <footer>
	<div style="text-align:center;border:0px solid red">
	  <h4>______________</h4>
	  <h4>Copyright © 2021 <a href="https://weedigital.vn/">WeeDigital</a>. All rights reserved.</h4>
	</div>
  </footer>
</html>
`

/* HTML content */
const page404 string = `
<!DOCTYPE html>
<html lang="vi-VI" data-oe-company-name="WeeDigital Company">
  <head>
    <meta charset="utf-8"/>
    <title>PAGE NOT FOUND | 404</title>
    <link rel="alternate" hreflang="vi" href="/page404"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
  </head>
  <body>
    <div>
      </br>
      <div style="text-align:center;border:0px solid red">
        </br>
        <h1>404</h1>
		<h4>PAGE NOT FOUND</h4>
      </div>
    </div>
  </body>
  <footer>
	<div style="text-align:center;border:0px solid red">
	  <h4>______________</h4>
	  <h4>Copyright © 2021 <a href="https://weedigital.vn/">WeeDigital</a>. All rights reserved.</h4>
	</div>
  </footer>
</html>
`
