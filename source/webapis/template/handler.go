package template

import (
	"app/libs/zapc"
	"encoding/base64"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	favicon []byte
	log     zapc.Service
}

func New(log zapc.Service) *Handler {
	img, err := base64.StdEncoding.DecodeString(faviconIco)
	if err != nil {
		log.Error("base64.DecodeString", zapc.Error(err))
	}
	return &Handler{
		favicon: img,
		log:     log,
	}
}

/*
AddRoutes routers to engine
*/
func (ins *Handler) AddRoutes(r *gin.Engine) {

	r.Handle(http.MethodGet, "/favicon.ico", ins.getFavicon)
	r.Handle(http.MethodGet, "/health", ins.health)
	r.NoRoute(ins.pageNotFound)
}

func (ins *Handler) getFavicon(c *gin.Context) {
	c.Writer.Write(ins.favicon)
}

func (ins *Handler) health(c *gin.Context) {
	c.Writer.WriteString(healthy)
}

func (ins *Handler) pageNotFound(c *gin.Context) {
	c.Writer.WriteString(page404)
}
