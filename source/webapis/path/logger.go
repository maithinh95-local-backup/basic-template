package path

import (
	"app/libs/zapc"
	"app/source/webapis/models"
	"context"
	"time"
)

type logger struct {
	next    Service
	console zapc.Service
}

func NewServiceAsLogger(zaplog zapc.Service) Service {
	return &logger{
		next:    newService(),
		console: zaplog,
	}
}

func (ins *logger) action(ctx context.Context, request actionReq, response *actionResp) (statusCode int, err error) {
	defer func(begin time.Time) {
		var (
			metric = time.Since(begin)
			path   = "/feature/phoneUpdate"
		)
		{
			response.Metric = 1e-6 * float64(metric)
		}
		/*
		 ========= logging here ... ===========
		*/
		ins.console.WithFields(
			zapc.Any(models.IsKeyRequest, request),
			zapc.Any(models.IsKeyResponse, response),
			zapc.Any(models.IsKeySession, ctx.Value(models.KeySessionContext)),
		).Info(path, zapc.Duration(models.IsKeyAt, metric))
		if err != nil {
			ins.console.WithFields(zapc.Any(models.IsKeySession, ctx.Value(models.KeySessionContext))).WithError(path, err)
		}
	}(time.Now())
	return ins.next.action(ctx, request, response)
}
