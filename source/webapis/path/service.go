package path

import (
	"app/source/webapis/models"
	"context"
)

type service struct {
}

type Service interface {
	action(ctx context.Context, request actionReq, response *actionResp) (int, error)
}

func newService() Service {
	return &service{}
}

func (ins *service) action(ctx context.Context, request actionReq, response *actionResp) (int, error) {
	var (
		err error
	)

	response.APIError, err = models.NewAPIError(1, "test", 0, nil, "ok")

	return 200, err
}
