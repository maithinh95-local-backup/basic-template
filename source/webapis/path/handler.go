package path

import (
	"app/libs/net"
	"app/libs/zapc"
	"app/source/webapis/models"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Handler struct {
	service Service
	zapc    zapc.Service
}

func New(zapraw zapc.Service, srv Service) *Handler {
	return &Handler{
		service: srv,
		zapc:    zapraw,
	}
}

func (ins *Handler) AddRouter(r *gin.Engine) {
	networkMiddleware := func(zapc zapc.Service, next func(net.Net, *gin.Context)) gin.HandlerFunc {
		return func(c *gin.Context) {
			var (
				network = net.NewNetwork(primitive.NewObjectID().Hex(), c.Request, c.Writer)
			)
			defer network.WriteLog(zapc)
			next(network, c)
		}
	}

	r.Handle(http.MethodGet, "/path/entry", networkMiddleware(ins.zapc, ins.handlerFunc))

}

func (ins *Handler) handlerFunc(network net.Net, c *gin.Context) {
	var (
		err error
		ctx = context.WithValue(c.Request.Context(), models.KeySessionContext, network.GetSeviceName())

		request  = actionReq{}
		response = actionResp{}
	)

	status, err := ins.service.action(ctx, request, &response)
	if err != nil {
		return
	}

	network.WriteJSON(status, response)
}
