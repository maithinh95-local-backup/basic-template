package models

import (
	"strings"

	"github.com/pkg/errors"
)

/*
APIError reponse object
*/
type APIError struct {
	Code         errCode `json:"code"`
	Metric       float64 `json:"metric"`
	Message      string  `json:"message"`
	ReturnIndex  int     `json:"returnIndex"`
	ReturnAction string  `json:"returnAction"`
}

type errCode int

/*
Error code constants
*/
const (
	Success     errCode = iota      // 0
	NotModified errCode = iota + 33 // 34
	_
	_
	_
	_
	_
	BadRequest                           // 40
	ParameterInvalid errCode = iota + 32 // 40
	_
	_
	_
	_
	_
	_
	_
	_
	_
	InternalServerError // 50
	NotImplemented      // 51
	BadGateway          // 52
	ServiceUnavailable  // 53
	GateWayTimeout      // 54
)

/*
GetMessageError function
*/
func GetMessageError(code errCode) string {
	switch code {
	case Success:
		return "SUCCESSFULLY"
	case NotModified:
		return "NOT_MODIFIED"
	case BadRequest:
		return "PARAMETER_INVALID"
	case InternalServerError:
		return "INTERNAL_SERVER_ERROR"
	case NotImplemented:
		return "NOT_IMPLEMENTED"
	case BadGateway:
		return "BAD_GATEWAY"
	case ServiceUnavailable:
		return "SERVICE_UNAVAILABLE"
	case GateWayTimeout:
		return "GATEWAY_TIMEOUT"
	default:
		return "UNKNOWN_ERROR"
	}
}

/*
NewAPIError function
*/
func NewAPIError(index int, action string, code errCode, err error, msgs ...string) (APIError, error) {
	if len(msgs) == 0 {
		msgs = append(msgs, GetMessageError(code))
	}
	return APIError{
		Code:         code,
		Message:      strings.Join(msgs, ", "),
		ReturnIndex:  index,
		ReturnAction: action,
	}, errors.WithStack(err)
}
