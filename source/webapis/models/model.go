package models

type SessionContext string

const (
	IsKeyRequest  string = "request"
	IsKeyResponse string = "response"
	IsKeyAt       string = "at"
	IsKeySession  string = "session"

	KeySessionContext SessionContext = "session"

	UserTokenEmpty     string = "usertoken must be not empty"
	PhoneSuffixInvalid string = "phone must be hash256 string"
	FileIsTooShort     string = "file is too short"
)
