package main

import (
	"app"
	"app/configuration"
	"app/libs/monitor"
	"app/libs/zapc"
	"app/source/webapis/path"
	"app/source/webapis/template"
	"context"
	"io"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	var (
		pid  string = monitor.New().GetPID().String()
		info        = app.GetBuiltInfomation().ShowDisplay()

		setting = configuration.New().GetSetting()

		ioc     zapc.Service = zapc.New(pid, zapc.DebugLevel, zapc.JSONEncoder, zapc.NewRotateWriter(setting.LogToFile, "io.log"))
		console zapc.Service = zapc.New(pid, zapc.DebugLevel, zapc.JSONEncoder, zapc.NewRotateWriter(setting.LogToFile, "console.log"))

		root      *gin.Engine    = gin.New()
		interrupt chan os.Signal = make(chan os.Signal)
		ctx       context.Context
		cancel    context.CancelFunc

		pathservice = path.NewServiceAsLogger(console)
	)

	{
		console.Info("/", zapc.Any("BUILT INFO", info))
	}

	{
		handlerFunc := cors.New(cors.Config{
			AllowAllOrigins:  true,
			AllowMethods:     []string{http.MethodGet, http.MethodPost, http.MethodOptions},
			AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "*"},
			AllowCredentials: false,
			AllowWebSockets:  true,
			MaxAge:           12 * time.Hour,
		})

		loggerFunc := gin.LoggerWithWriter(
			io.MultiWriter(os.Stdout, zapc.NewRotateWriter(setting.LogToFile, "network.log")),
		)

		root.Use(handlerFunc, loggerFunc)

		template.New(console).AddRoutes(root)
		path.New(ioc, pathservice).AddRouter(root)
	}

	server := &http.Server{
		Addr:         setting.WhoAmI.Domain.ToAddr(), //  "0.0.0.0:8080",
		Handler:      root,
		ReadTimeout:  setting.WhoAmI.ReadTimeout,  // 15 * time.Second,
		WriteTimeout: setting.WhoAmI.WriteTimeout, // 15 * time.Second,
		IdleTimeout:  setting.WhoAmI.IdleTimeout,  // 60 * time.Second,
	}

	go func() {
		println("[SERVER---] Start on ", server.Addr)
		console.Info("Server startting ...", zapc.String("addr", server.Addr), zapc.Time("at", time.Now()))

		if err := server.ListenAndServe(); err != nil {
			println("[SERVER---]", err.Error())
			console.Info("ListenAndServe", zapc.Error(err))
		}
		println("[SERVER---] Shutdown !")
		console.Info("Server has shutdown", zapc.Time("at", time.Now()))
		interrupt <- os.Interrupt
	}()

	{
		signal.Notify(interrupt, os.Interrupt)
		<-interrupt

		ctx, cancel = context.WithTimeout(context.TODO(), 5*time.Second)
		defer cancel()
		server.Shutdown(ctx)
		os.Exit(0)
	}
}
